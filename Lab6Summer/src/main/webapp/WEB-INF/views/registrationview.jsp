<%-- 
    Document   : registrationview
    Created on : Jun 2, 2016, 11:01:32 PM
    Author     : Dev
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <spring:form id="myForm" action="registerUser.htm" method="POST" commandName="user">
                First : <spring:input path="first"/><br><br>

                Last : <spring:input path="last"/><br><br>

                Gender:
                <spring:radiobutton path="gender" value="M" checked="true"/>Male
                <spring:radiobutton path="gender" value="F"/>Female
                <br><br>

                Email : <spring:input path="email" type="text"/><br><br>

                About Me: <spring:textarea path="aboutMe"></spring:textarea> <br><br>

                <input type="submit" name="Register User">

            </spring:form>
    </body>
</html>
