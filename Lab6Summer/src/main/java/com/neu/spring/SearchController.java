package com.neu.spring;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.neu.spring.dao.UserDao;
import com.neu.spring.pojo.User;

@Controller
public class SearchController{
    
	@RequestMapping(method=RequestMethod.POST,value="/searchUser.htm")
    public ModelAndView handleRequest(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        
        String action = hsr.getParameter("action");
        UserDao userDao = new UserDao();
        if (action.equalsIgnoreCase("searchuser")) {
        	
            List<User> userList = new ArrayList();

            String key = hsr.getParameter("key");
            String searchKey = hsr.getParameter("flag");

            if (searchKey.equalsIgnoreCase("gender")) {
                if (key.equalsIgnoreCase("Male") || key.equalsIgnoreCase("M")) {
                    key = "M";
                }

                if (key.equalsIgnoreCase("Female") || key.equalsIgnoreCase("F")) {
                    key = "F";
                }
            }
            
            
            userList = userDao.searchUsers(key, searchKey);
            JSONObject obj = new JSONObject();
            obj.put("users",userList);
            PrintWriter out = hsr1.getWriter();
            out.println(obj);
        }
        
        if(action.equals("delete")){
            int id = Integer.parseInt(hsr.getParameter("user"));
            userDao.deleteUser(id);
        }

        return null;
    }

}
