package com.neu.spring;

import java.io.File;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.neu.spring.dao.UserDao;
import com.neu.spring.pojo.User;
import com.neu.spring.validators.RegistrationValidator;


import org.json.JSONObject;

@Controller
public class UserController {

	
	@Autowired
	@Qualifier("regValidator")
	private RegistrationValidator registrationValidator;
   
	@InitBinder
	private void initBinder(WebDataBinder binder)
	{
		binder.setValidator(registrationValidator);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String getHomePage(){
		return "index";
	}
	
	@RequestMapping(method = RequestMethod.GET,value="/registerUser.htm")
	public String getRegistrationPage(@ModelAttribute("user")User user){
		return "registrationview";
	}
	@RequestMapping(method=RequestMethod.POST,value="/registerUser.htm")
    protected String registerUser(@ModelAttribute("user")User user,BindingResult result) throws Exception{

		registrationValidator.validate(user, result);
		if(result.hasErrors())
		{
			return "registrationview";
		}
		
		UserDao userDao = new UserDao();
		userDao.addUser(user);
		return "success";
    }

}
