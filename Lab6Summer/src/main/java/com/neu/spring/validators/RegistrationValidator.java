package com.neu.spring.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import com.neu.spring.pojo.User;



public class RegistrationValidator implements Validator {
    
    private static final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";

   @Override
   public boolean supports(Class clazz) {
      
       return User.class.equals(clazz);
   }

   @Override
   public void validate(Object object, Errors errors) {
       
       Pattern pattern = Pattern.compile(IMAGE_PATTERN);
       Matcher matcher;
       MultipartFile photo;
      
       ValidationUtils.rejectIfEmpty(errors, "first","Test", "Field cannot be empty");
       ValidationUtils.rejectIfEmpty(errors, "last","Test", "Field cannot be empty");
       ValidationUtils.rejectIfEmpty(errors, "gender","Test", "Field cannot be empty");
       ValidationUtils.rejectIfEmpty(errors, "aboutMe","Test", "Field cannot be empty");
       ValidationUtils.rejectIfEmpty(errors, "email","Test", "Field cannot be empty");
       
       
      
       
       
      
       
   }
   
}
