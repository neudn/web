<%-- 
    Document   : signup
    Created on : Feb 11, 2016, 11:49:56 PM
    Author     : kumaran_jay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sign up</title>
    </head>
    <body>
    <script>
       
       

        //AJAX

        var xmlHttp;
        xmlHttp = GetXmlHttpObject();
        function checkUser() {
           if (xmlHttp == null)
            {
                alert("Your browser does not support AJAX!");
                return;
            }
            var username = document.getElementById("username").value;
            var query = "action=ajaxCheck&username="+username;

            xmlHttp.onreadystatechange = function stateChanged()
            {
                if (xmlHttp.readyState == 4)
                {
                    //alert(xmlHttp.responseText);
                    //var json = JSON.parse(xmlHttp.responseText);
                    document.getElementById("error").innerHTML = xmlHttp.responseText;
                }
            };
            xmlHttp.open("POST", "signup.htm", true);
            xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlHttp.send(query);
           return false;
        }
        
         function GetXmlHttpObject()
        {
            var xmlHttp = null;
            try
            {
                // Firefox, Opera 8.0+, Safari
                xmlHttp = new XMLHttpRequest();
            } catch (e)
            {
                // Internet Explorer
                try
                {
                    xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
                } catch (e)
                {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
            }
            return xmlHttp;
        }
        
        </script>
        
        <h1>Sign up</h1>
        <form action="signup.htm?action=signup" method="post">
            <p>Name: <input type="text" name="name" >
            </p>
            <p>City: <input type="text" name="city"></p>
            <p>Phone: <input type="text" name="number"></p>
            <p>Username: <input type="text" name="username" id="username" onblur="return checkUser()">
            <div id="error" style="color:red"></div></p>
            <p>Password: <input type="password" name="password"></p>
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>
