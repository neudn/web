<%-- 
    Document   : registrationview
    Created on : Jun 2, 2016, 11:01:32 PM
    Author     : Dev
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h4>Create User Form</h4>
        
       <spring:form commandName="user" onsubmit="return myFunction();" method="POST" enctype="multipart/form-data">
            First : <spring:input path="first" id="first" type="text"/>
            <spring:errors cssStyle="color:red" path="first"/><br><br>
            
            Last : <spring:input path="last" id="last" type="text"/>
            <spring:errors cssStyle="color:red" path="last"/><br><br>
            
            Gender:
            <spring:radiobutton path="gender" id="gender" value="M"/>Male
            <spring:radiobutton path="gender" id="gender" value="F"/>Female
            <spring:errors cssClass="color:red" path="gender"/><br><br>
            
            Email : <spring:input path="email" id="email" type="text"/>
            <spring:errors cssStyle="color:red" path="email"/><br><br>
            
            About Me: <spring:textarea path="aboutMe" id="aboutMe"/>
            <spring:errors cssStyle="color:red" path="aboutMe"/> <br><br>
            
            Select Photo (Max Size: 5MB): <spring:input path="photo" type="file"/>
            <spring:errors cssStyle="color:red" path="photo"/> <br><br>
            
            <input type="submit" name="Register User">
            
        </spring:form>
    </body>
</html>
