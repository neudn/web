package com.neu.controller;

import com.neu.dao.UserDao;
import com.neu.pojo.User;
import java.io.File;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.json.JSONObject;

/**
 *
 * @author Dev
 */
public class UserFormController extends SimpleFormController {

    private UserDao userDao;

    public UserFormController() {

    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    protected void doSubmitAction(Object command) throws Exception {
        User user = (User) command;
        File file;
        String check = File.separator; //Checking if system is linux based or windows based by checking seprator used.
        String path = null;
        if (check.equalsIgnoreCase("\\")) {
            path = getServletContext().getRealPath("").replace("build\\", ""); //Netbeans projects gives real path as Lab6/build/web/ so we need to replace build in the path.
            path += "\\";
        }

        if (check.equalsIgnoreCase("/")) {
            path = getServletContext().getRealPath("").replace("build/", "");
            path += "/"; //Adding trailing slash for Mac systems.

        }

        if (user.getPhoto() != null) {

            String fileNameWithExt = System.currentTimeMillis() + user.getPhoto().getOriginalFilename();
            file = new File(path + fileNameWithExt);
            String context = getServletContext().getContextPath();

            user.getPhoto().transferTo(file);
            user.setPhotoName(context + "/" + fileNameWithExt);
            getUserDao().addUser(user);

        }

    }

}
