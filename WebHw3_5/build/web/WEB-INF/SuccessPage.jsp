<%-- 
    Document   : SuccessPage
    Created on : Jun 5, 2016, 12:42:16 AM
    Author     : dn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <h1>${requestScope.number} Book Added Successfully!</h1>
        <input type = "hidden" name = "action" value = null>
        <a href = "/WebHw3_5/">Click here to go back to the main page</a>
    </body>
</html>
