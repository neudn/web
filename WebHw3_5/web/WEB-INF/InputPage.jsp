<%-- 
    Document   : InputPage
    Created on : Jun 10, 2016, 1:37:54 PM
    Author     : dn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>JSP Page</title>
    </head>
    <body>
        <form action = "input.htm" method = "post">
        <table id="myTable">
            <tr>
                <td>ISBN</td>
                <td>Title</td>
                <td>Author</td>
                <td>Price</td>
            </tr>
    <%
        for(int i = 0; i < Integer.parseInt(request.getParameter("number")); i++){
            out.println("<tr>"
                    + "<td><input type = 'text'/ name = 'cell'></td>"
                    + "<td><input type = 'text'/ name = 'cell'></td>"
                    + "<td><input type = 'text'/ name = 'cell'></td>"
                    + "<td><input type = 'text'/ name = 'cell'></td>"
                    + "</tr>");
        }
    %>   
        </table>
        <input type = "submit" value = "Add Books"/>
        <input type = "hidden" name = "action" value = "info"/>
        </form>
    </body>
</html>
