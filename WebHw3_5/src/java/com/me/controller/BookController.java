/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.controller;

import com.me.DAO.DAO;
import com.me.pojo.Book;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dn
 */
@WebServlet(name = "BookController", urlPatterns = {"/BookController"})
public class BookController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            if(action == null){
                RequestDispatcher rd = request.getRequestDispatcher("index.html");
                rd.forward(request, response);
            } else if(action.equals("input")){
 //               String number = request.getParameter("number");
 //               int numberOfBooks = Integer.parseInt(number);
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/InputPage.jsp");
                rd.forward(request, response);
            } else if(action.equals("info")){ 
                String[] cells = request.getParameterValues("cell");
//                for(String str: cells) System.out.println(str);
                int num = cells.length / 4;
                request.setAttribute("number", cells.length/4);
        
                String dburl = "jdbc:mysql://localhost:3306/test";
                String dbuser = "root";
                String dbpassword = "root"; 
                String dbdriver = "com.mysql.jdbc.Driver";
                
                Connection conn = DAO.getConnectionJDBC(dburl, dbuser, dbpassword, dbdriver);
                
                for(int i = 0; i < cells.length; i = i+4){
                    
                    String queryInsert = "insert into books values("
                        + "\'" + cells[i] + "\',"
                        + "\'" + cells[i+1] + "\',"
                        + "\'" + cells[i+2] + "\',"
                        +  cells[i+3]
                        +");";
                     System.out.println(queryInsert);
                    PreparedStatement stmtInsert = conn.prepareStatement(queryInsert);
                    int executeUpdate = stmtInsert.executeUpdate();
                    stmtInsert.close();
                }
                             
                conn.close();
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/SuccessPage.jsp");
                rd.forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(BookController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(BookController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
