<%-- 
    Document   : home
    Created on : May 20, 2016, 10:23:31 PM
    Author     : kothari
--%>

<%@page import="java.lang.String"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neu.model.Course"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Course Registration</title>
        
        <script language="javascript">
            function showForm(){               
                var searchForm = document.getElementById("searchForm");
                if(searchForm.style.display == "none"){
                    searchForm.style.display = "block";
                }               
                else {
                    searchForm.style.display = "none";
                }               
            }           
        </script>
    </head>
    
    <body>
        
        <a href="courseShowAll.htm?action=showAll">Show courses</a><br>
        <a href="showMyCourses.htm?action=showMyCourses">View my courses</a><br />
        <a href="#" onclick="showForm()">Search Course</a><br />
        
        <div id ="searchForm" style="display: none">
            <h2> Course Search</h2>
            <form action="courseSearch.htm" method="get">
                Course Title: <input type="text" name="courseName"/> <br/><br/>
                Instructor: <input type="text" name="instName"/> <br/><br/>
                <input type="hidden" name="action" value="search" />
                <input type="submit" value="Search Courses"/>
                
            </form>
            
         </div>
        <%
            String taskValue = (String)request.getAttribute("task");
            if(taskValue!= null){
                if(taskValue.equalsIgnoreCase("show")){
            ArrayList<Course> outputCourses = (ArrayList<Course>)request.getAttribute("outputCourses");
            out.println("<hr />");
            out.println("<form method='post' action='addCourse.htm'>");
            out.println("<h2> Results found </h2>");
            for(Course course:outputCourses)
            {
                String crn=course.getCrn();
                String courseName=course.getName();
                
                
                out.println("<input type='checkbox' name='courseId' value='"+crn+"'/>" + crn + courseName +"<a href='addCourse.htm?crn="+crn+"&name="+courseName+"&action=single'> Add this course</a></h1><br />");
            }
            out.println("<input type='hidden' name='action' value='addMultiple'/>");
            out.println("<input type='submit' value='Add Selected Courses'/>");
            out.println("</form>");
           }
        else if(taskValue.equalsIgnoreCase("myCourses")){
            ArrayList<Course> outputCourses = (ArrayList<Course>)session.getAttribute("outputCourses");
            out.println("<hr />");
            out.println("<h3>Registered Courses are:</h3>");
            if(outputCourses == null){
                out.println("No courses registered");
            }
            else {
            
            
            for(Course course:outputCourses)
            {
                String crn=course.getCrn();
                String courseName=course.getName();
                
                
                out.println("<h1>"+courseName+":crn = "+crn+"</h1>");
            }
           }
            
           }
        }
        %> 
        
        
    </body>
</html>
