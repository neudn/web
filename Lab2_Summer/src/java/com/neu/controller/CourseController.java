/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neu.controller;

import com.neu.model.Course;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author kothari
 */
public class CourseController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        
        if (action != null) {
          if (action.equalsIgnoreCase("search")) {
                List<Course> listofcourses = CourseDb.getCoursesAvailable();
                List<Course> outputCourses = new ArrayList<Course>();

                String instructorName = request.getParameter("instName");
                String courseTitle = request.getParameter("courseName");

                instructorName = instructorName.replaceAll("[^\\dA-Za-z ]", "").replaceAll("\\s+", "+").trim();
                courseTitle = courseTitle.replaceAll("[^\\dA-Za-z ]", "").replaceAll("\\s+", "+").trim();

                if (instructorName.equals("") && courseTitle.equals("")) {
                    request.setAttribute("error", "true");
                    request.setAttribute("message", "Please enter valid instructor name and title");
                    RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/home.jsp");
                    rd.forward(request, response);
                }

                for (Course course : listofcourses) {
                    if (course.getInstructor().equalsIgnoreCase(instructorName) || course.getName().equalsIgnoreCase(courseTitle)) {
                        outputCourses.add(course);
                    }
                }

                request.setAttribute("outputCourses", outputCourses);
                request.setAttribute("task", "show");
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/home.jsp");
                rd.forward(request, response);
            } 
          
          else if (action.equalsIgnoreCase("showAll")) {
                List<Course> listofcourses = CourseDb.getCoursesAvailable();
                request.setAttribute("outputCourses", listofcourses);
                request.setAttribute("task", "show");
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/home.jsp");
                rd.forward(request, response);

            } //
            
            else if (action.equalsIgnoreCase("showMyCourses")) {

                request.setAttribute("task", "myCourses");
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/home.jsp");
                rd.forward(request, response);

            } 
            else if (action.equalsIgnoreCase("addMultiple")) {
                HttpSession session = request.getSession();
                ArrayList<Course> myCourses;
                if (null != session.getAttribute("outputCourses")) {
                    myCourses = (ArrayList<Course>) session.getAttribute("outputCourses");

                } else {
                    myCourses = new ArrayList<Course>();
                }

                String[] selectedCourses = request.getParameterValues("courseId");
                List<Course> courseList = CourseDb.getCoursesAvailable();

                for (int i = 0; i < selectedCourses.length; i++) {
                    for (Course course : courseList) {
                        if (course.getCrn().equals(selectedCourses[i])) {
                            if (!myCourses.contains(course)) {
                                myCourses.add(course);
                            }
                            
                        }
                    }
                }
                session.setAttribute("outputCourses",myCourses);
                request.setAttribute("task", "myCourses");
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/home.jsp");
                rd.forward(request, response);

            }
//        
        }
        
        else {
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/home.jsp");
            rd.forward(request, response);
            
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
