package com.me.system.dao;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.me.system.model.Contacts;

public class ContactsDAO extends DAO{
public ContactsDAO(){
	super();
}

public ArrayList<Contacts> listContactsByUsername(String username) throws Exception{
	try{
	ArrayList<Contacts> contactsList = new ArrayList<Contacts>();
	Query q = getSession().createQuery("from Contacts where userName = :username");
	q.setString("username", username);
	contactsList = (ArrayList<Contacts>) q.list();
	return contactsList;
	}catch(HibernateException e){
		throw new Exception("Could not get user", e);
	}
}

public void deleteContactsByID(String contactsID) throws Exception{
	try{
	Query q = getSession().createQuery("delete Contacts where contactsID = :contactsid");
    q.setString("contactsid", contactsID);
    q.executeUpdate();
	}catch(HibernateException e){
		throw new Exception("Could not get user ", e);
	}
}
}
