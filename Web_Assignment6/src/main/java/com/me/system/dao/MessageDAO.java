package com.me.system.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.me.system.model.Message;

public class MessageDAO extends DAO{
     public MessageDAO(){
    	 super();
     }
     
     public ArrayList<Message> listMessageByUsername(String username) throws Exception{
    	 ArrayList<Message> messageList = new ArrayList<Message>();
    	 try{
    		 Query q = getSession().createQuery("from Message where userName = :username");
    	     q.setString("username", username);
    	     
			messageList = (ArrayList<Message>) q.list();
    	     return messageList;
    	 }catch(HibernateException e){
    		 throw new Exception("Could not get user", e);
    	 }
     }
     
     public void deleteMessage(String messageID) throws Exception{
    	 try{
    		 Query q = getSession().createQuery("delete Message where messageID = :messageID");
    	     q.setString("messageID", messageID);
    	     q.executeUpdate();
    	 }catch(HibernateException e){
    		 throw new Exception("Could not get User", e);
    	 }
     }
     
     //public void addMessage(String username, String toUser, String message, String messageDate) throws Exception{
    	 //try{
    		// Query q = getSession().createQuery("insert into Message (userName, fromUser, message, messageDate)" + "select :userName, :fromUser, :message, :messageDate");
    	 //q.setString("userName", toUser);
    	 //q.setString("fromUser", username);
    	 //q.setString("message", message);
    	 //q.setString("messageDate", messageDate);
    	// }catch(HibernateException e){
    		 //throw new Exception("Could not get User" + username, e);
    	 //}
     //}
}
