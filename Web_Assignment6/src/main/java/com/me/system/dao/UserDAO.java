package com.me.system.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.me.system.model.User;

public class UserDAO extends DAO{
    public User queryUserByNameAndPassword(String username, String password) throws Exception{
    	try{
    		Query q = getSession().createQuery("from User where username = :username and password = :password");
    	    q.setString("username", username);
    	    q.setString("password", password);
    	    User user = (User) q.uniqueResult();
    	    return user;
    	} catch(HibernateException e){
    		throw new Exception("Counld not get user" + username, e);
    	}
    	
    }
    
    public User listUser(String username) throws Exception{
    	try{
    		Query q = getSession().createQuery("from User where username = :username");
    		q.setString("username", username);
    		User user = (User)q.uniqueResult();
    		return user;
    	}catch(HibernateException e){
    		throw new Exception("Could not get user" + username, e);
    	}
    }
}
