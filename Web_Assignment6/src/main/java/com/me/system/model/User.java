package com.me.system.model;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;

@Scope("session")
public class User implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private long id;
	private String username;
	private String password;
	private String gender;
    private String city;
    private String state;
    private String country;
    private String aboutme;
    
    
    
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAboutme() {
		return aboutme;
	}
	public void setAboutme(String aboutme) {
		this.aboutme = aboutme;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    
}
