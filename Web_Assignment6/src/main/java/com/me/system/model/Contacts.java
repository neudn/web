package com.me.system.model;

import java.io.Serializable;

public class Contacts implements Serializable{
 private static final long serialVersionUID = 1L;
 
 private long contactID;
 private String userName;
 private String contactName;
 private String comments;
 private String dateAdded;

 
public long getContactID() {
	return contactID;
}
public void setContactID(long contactID) {
	this.contactID = contactID;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getContactName() {
	return contactName;
}
public void setContactName(String contactName) {
	this.contactName = contactName;
}
public String getComments() {
	return comments;
}
public void setComments(String comments) {
	this.comments = comments;
}
public String getDateAdded() {
	return dateAdded;
}
public void setDateAdded(String dateAdded) {
	this.dateAdded = dateAdded;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}
 
}
