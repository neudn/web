package com.me.system;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.me.system.dao.ContactsDAO;
import com.me.system.dao.MessageDAO;
import com.me.system.dao.UserDAO;
import com.me.system.model.Contacts;
import com.me.system.model.Message;
import com.me.system.model.User;

/**
 * Handles requests for the application home page.
 */
@Controller
@Scope("session")
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@Autowired
	@Qualifier("userValidator")
	private Validator validator;
	
	@Autowired
	private UserDAO userDao;
	private MessageDAO messageDao;
	private ContactsDAO contactsDao;
	
	@InitBinder
	private void initBinder(WebDataBinder binder){
		binder.setValidator(validator);
	}
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String initUserLoginForm(Model model, HttpServletRequest request) throws Exception{
		Cookie[] cookies = request.getCookies();
		String usernameCookie = "";
		String passwordCookie = "";
		for(Cookie cookie: cookies){
			if(cookie.getName().equals("usernameCookie")){
				usernameCookie = cookie.getValue();
			}
			if(cookie.getName().equals("passwordCookie")){
				passwordCookie = cookie.getValue();
			}
		}
		
		User u = userDao.queryUserByNameAndPassword(usernameCookie, passwordCookie);
		if(u != null){
			try{
				ArrayList<Message> messageList = messageDao.listMessageByUsername(u.getUsername());
				ArrayList<Contacts> contactsList = contactsDao.listContactsByUsername(u.getUsername());
				model.addAttribute("user", u);
				model.addAttribute("messageList", messageList);
				model.addAttribute("contactsList", contactsList);
				int messageSize = messageList.size();
				int contactsSize = contactsList.size();
				model.addAttribute("messageSize", messageSize);
				model.addAttribute("contactsSize", contactsSize);
				HttpSession session = request.getSession();
	            session.setAttribute("username", u.getUsername());
	            return "menu";
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		else{
			User user = new User();
			model.addAttribute("user", user);
		}
		return "home";
	}
	
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String submitForm(Model model, @Validated User user, BindingResult result, HttpServletRequest request, HttpServletResponse response) throws Exception{
		model.addAttribute("user", user);
		String returnVal = "menu";
		String remember = request.getParameter("remember");
		if(result.hasErrors()){
			return "home";
		}else{
			try{
				User u = userDao.queryUserByNameAndPassword(user.getUsername(), user.getPassword());
			    ArrayList<Message> messageList = messageDao.listMessageByUsername(u.getUsername());
				ArrayList<Contacts> contactsList = contactsDao.listContactsByUsername(user.getUsername());
				int messageSize = messageList.size();
				int contactsSize = contactsList.size();
				if(u != null){
					if(remember != null){
						Cookie usernameCookie = new Cookie("usernameCookie", u.getUsername());
						Cookie passwordCookie = new Cookie("passwordCookie", u.getPassword());
					    usernameCookie.setMaxAge(604000);
					    usernameCookie.setMaxAge(604000);
					    response.addCookie(usernameCookie);
					    response.addCookie((passwordCookie));
					}
					
					model.addAttribute("user", u);
					model.addAttribute("messageList", messageList);
					model.addAttribute("contactsList", contactsList);
					model.addAttribute(messageSize);
					model.addAttribute(contactsSize);
					HttpSession session = request.getSession();
					session.setAttribute("username", u.getUsername());
					return returnVal;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return "home";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET) 
	public String logout(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		Cookie[] cookies = request.getCookies();
		if(cookies != null){
			for(int i = 0; i<cookies.length; i++){
				cookies[i].setValue("");
				cookies[i].setPath("/");
				cookies[i].setMaxAge(0);
				response.addCookie(cookies[i]);
			}
		}
		return initUserLoginForm(model, request);
	}
}
