package com.me.system;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.me.system.dao.MessageDAO;
import com.me.system.dao.UserDAO;
import com.me.system.model.User;

@Controller
@Scope("session")
public class SearchController {

	
	@Autowired
	private UserDAO userDao;
	@Autowired
	private MessageDAO messageDao;
	
	@RequestMapping(value ="/search", method = RequestMethod.POST)
	public String searchUsers(Model model, HttpServletRequest request){
		try{
			String username = request.getParameter("search");
			User user = userDao.listUser(username);
			model.addAttribute("user", user);
			return "searchResult";
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
