package com.me.system;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.me.system.dao.HibernateUtil;
import com.me.system.model.Contacts;
import com.me.system.model.Message;
import com.me.system.model.User;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.me.system.dao.ContactsDAO;
import com.me.system.dao.MessageDAO;
import com.me.system.dao.UserDAO;

@Controller
@Scope("session")
public class ResponseController {

	
	@Autowired
	private UserDAO userDao;
	@Autowired
	private MessageDAO messageDao;
	@Autowired
	private ContactsDAO contactsDao;
	
	@RequestMapping(value = "/reply", method= RequestMethod.GET)
	public String replyMessage(Model model, @RequestParam("toUser") String toUser, HttpServletRequest request){
		HttpSession session = request.getSession();
		String username = (String)session.getAttribute("username");
		
		model.addAttribute("toUser", toUser);
		model.addAttribute("username", username);
		return "reply";
	}
	
	
	@RequestMapping(value = "/delete", method=RequestMethod.POST)
	public String deleteMessage(Model model, HttpServletRequest request) throws Exception{
		String messageIDArr[] = request.getParameterValues("delete");
		if(messageIDArr != null){
			for(int i=0; i < messageIDArr.length; i++){
				messageDao.deleteMessage(messageIDArr[i]);
			}
		}
		return "deleteMessages";
	}
	
	@RequestMapping(value = "/replyMessage", method=RequestMethod.POST)
	public String addMessage(Model model, HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
		String username = (String)session.getAttribute("username");
		String message = request.getParameter("message");
		String toUser = request.getParameter("toUser");
		
		Session hibernateSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = hibernateSession.beginTransaction();
		
		Message insertMessage = new Message();
		insertMessage.setUserName(toUser);
		insertMessage.setFromUser(username);
		insertMessage.setMessage(message);
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String s = dateFormat.format(date);
		insertMessage.setMessageDate(s);
		hibernateSession.save(insertMessage);
        
		tx.commit();
		hibernateSession.close();
		
		model.addAttribute("toUser", toUser);
		model.addAttribute("username", username);
		return "success";
	}
	
	@RequestMapping(value = "/addContacts", method=RequestMethod.POST)
	public String addContacts(Model model, HttpServletRequest request) throws Exception{
		String contactName = request.getParameter("addUser");
		model.addAttribute("contactName", contactName);
		return "add";

	}
	
	@RequestMapping(value = "/askAddContacts", method=RequestMethod.POST)
	public String askAddContacts(Model model, HttpServletRequest request) throws Exception{
		HttpSession session = request.getSession();
        String username = (String)session.getAttribute("username");
        String message = request.getParameter("message");
        String contactName = request.getParameter("contactName");
        
		Session hibernateSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = hibernateSession.beginTransaction();
		
		Contacts insertContacts = new Contacts();
		insertContacts.setUserName(username);
		insertContacts.setContactName(contactName);
		insertContacts.setComments(message);
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String s = dateFormat.format(date);
		insertContacts.setDateAdded(s);
		hibernateSession.save(insertContacts);
		
		hibernateSession.close();
		tx.commit();
		return "addSuccess";
	}
	
	@RequestMapping(value = "/deleteContacts", method=RequestMethod.POST)
	public String deleteContacts(Model model, HttpServletRequest request) throws Exception{
		String contactIDArr[] = request.getParameterValues("remove");
		if(contactIDArr != null){
			for(int i=0; i<contactIDArr.length; i++){
				contactsDao.deleteContactsByID(contactIDArr[i]);
			}
		}
		return "deletedContacts";
	}
}
