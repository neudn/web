<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Menu</title>
        <style type="text/css">
            div#list{height:300px;width:100px;float:left;text-decoration: underline;color: blue}
            div#show{height:300px;width:450px;float:left}
            div#search{height:300px;width:450px;float:left}
            div#showmessages{height:300px;width:450px;float:left}
            div#viewcontacts{height:300px;width:450px;float:left}
            div#changepassword{height:300px;width:650px;float:left}
        </style>
</head>
<body>
<div id="list">
            <a onclick="displayPage('search')">Search</a><br><br>
            <a onclick="displayPage('showmessages')">Show Messages</a><br><br>
            <a onclick="displayPage('viewcontacts')">View My Contacts</a><br><br>
            <a onclick="displayPage('changepassword')">Change Password</a><br><br>
            <a href="logout">Logout</a><br><br>
        </div>
        
        <div id="show">
        <form action="delete" method="post">
        Welcome ${user.username}
        <table border="1">
        <tr>
        <th>MessageID</th>
        <th>From User</th>
        <th>Message</th>
        <th>Date</th>
        <th>Reply to User</th>
        <th>Delete</th>
        </tr>
        
        <c:forEach items = "${messageList}" var="message">
        <tr>
        <td align="center">${message.messageID}</td>
        <td align="center">${message.fromUser}</td>
        <td align="center">${message.message}</td>
        <td align="center">${message.messageDate}</td>
        <td align="center"><a href="reply?toUser=${message.fromUser}">Reply</a></td>
        <td align="center"><input type="checkbox" name="delete" value="${message.messageID}"/>delete</td>
        </tr>
        </c:forEach>
        <tr>
        <th>You have ${messageSize} messages</th>
        </tr>
        </table>
        <br><input type="submit" value="delete selected messages"/>
        </form>
        </div>
        
        <script type="text/javascript">
        var id_list = ["search", "showmessages", "viewcontacts", "changepassword", "logout"];
        function display(id){
            for(var i=0; i<id_list.length; i++){
             if(id_list[i] === id){
                     document.getElementById(id_list[i]).style.display = "inline";
                     document.getElementById('show').style.display = "none";
                 }
             else{
                 document.getElementById(id_list[i]).style.display = "none";
                 }
                }
            }
        </script>
        
        <div id="search" style="display: none">
        <form action="search" method="post">
        Look up by username<hr>
        <br><br>
        Please enter the username of the person you wnat to look up<br><br>
        <input type="hidden" name="page" value="search"/>
        <input type="text" name="search">
        <input type="submit" value="see profile" />
        </form>
        </div>
        
        <div id="showmessages" style="display: none">
        <form action="delete" method="post">
        Welcome ${user.username}
        <table border="1">
        <tr>
                        <th>Message ID</th>
                        <th>From User</th>
                        <th>Message</th>
                        <th>Date</th>
                        <th>Reply to User</th>
                        <th>Delete</th>
                    </tr>
                    
                    <c:forEach items = "${messageList}" var="message" >
                    <tr>
                        <td align="center">${message.messageID}</td>
                        <td align="center">${message.fromUser}</td>
                        <td align="center">${message.message}</td>
                        <td align="center">${message.messageDate}</td>
                        <td align="center"><a href="reply?toUser=${message.fromUser}">Reply</a></td>
                        <td align="center"><input type="checkbox" name="delete" value="${message.messageID}"/>delete</td>
                    </tr>
                    </c:forEach>
                    <tr>
                    	<td align="center">You have ${messageSize} messages</td>
                    </tr>
        </table>
        </form>
        </div>
        
        <div id="viewcontacts" style="display: none">
        <form action="deleteContacts" method="post">
        Welcome ${username}<hr>
        <input type="hidden" name="page" value="viewcontacts"/>
        <br><br>
        <table border="1">
        <tr>
                        <th>Contact ID</th>
                        <th>Contact Name</th>
                        <th>Comments</th>
                        <th>Data</th>
                        <th>Delete Contacts</th>
                    </tr>
                    <c:forEach items = "${contactsList}" var="contacts" >
                    <tr>
                        <td align="center">${contacts.contactID}</td>
                        <td align="center">${contacts.contactName}</td>
                        <td align="center">${contacts.comments}</td>
                        <td align="center">${contacts.dateAdded}</td>
                        <td align="center"><input type="checkbox" name="remove" value="${contacts.contactID}" />delete</td>
                    </tr>
                    </c:forEach>
        </table>
        <input type="submit" value="Delete selected contacts" />
        </form>
        </div>
        
        <div id="changepassword" style="display: none">
        <form action="changepassword" method="post">
        <p>Change password</p>
        Please Enter your old password: <input type= "text" name="oldpassword"/><br />
        Enter New password: <input type="text" name="newpassword" /><br />
        Enter New password Again: <input type="text" name="newpassword1" /><br />
        <input type="submit" value="change password" />
        </form>
        </div>
</body>
</html>